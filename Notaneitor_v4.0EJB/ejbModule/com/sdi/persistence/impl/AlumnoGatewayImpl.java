package com.sdi.persistence.impl;

import java.util.List;

import javax.persistence.EntityManager;
import com.sdi.model.Alumno;
import com.sdi.persistence.AlumnoGateway;

public class AlumnoGatewayImpl implements AlumnoGateway {
	
	@SuppressWarnings("unchecked")
	public List<Alumno> getAlumnos(EntityManager em) {
		return em.createNamedQuery("Alumnos.listar")
				.getResultList();
	}
}
