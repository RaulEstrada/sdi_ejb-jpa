package com.sdi.persistence.impl;

import com.sdi.persistence.AlumnoGateway;
import com.sdi.persistence.PersistenceFactory;

/**
 * Implementación de la factoria que devuelve instancia de la capa
 * de persistencia con Jdbc 
 * 
 * @author alb
 */
public class SimplePersistenceFactory implements PersistenceFactory {

	@Override
	public AlumnoGateway createAlumnoDao() {
		return new AlumnoGatewayImpl();
	}

}
