package com.sdi.persistence;

import java.util.List;

import javax.persistence.EntityManager;

import com.sdi.model.Alumno;

/**
 * Interfaz de la fachada a servicios de persistencia para la entidad Alumno.
 * 
 * En esta versi��n aparecen los otros m��todos b��sicos de un servicio 
 * de persistencia
 * 
 * @author alb
 *
 */
public interface AlumnoGateway {

	List<Alumno> getAlumnos(EntityManager em);

}