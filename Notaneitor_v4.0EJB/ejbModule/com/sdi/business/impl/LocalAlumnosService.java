package com.sdi.business.impl;

import javax.ejb.Local;

import com.sdi.business.AlumnosService;

@Local
public interface LocalAlumnosService extends AlumnosService {}
