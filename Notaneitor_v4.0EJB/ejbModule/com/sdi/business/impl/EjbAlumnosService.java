package com.sdi.business.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Alumno;
import com.sdi.persistence.AlumnoGateway;

/**
 * Clase de implementación (una de las posibles) del interfaz de la fachada de
 * servicios
 * 
 * @author alb
 * 
 */
@Stateless
public class EjbAlumnosService implements LocalAlumnosService, RemoteAlumnosService {
	@PersistenceContext(unitName="arenasforum")
	private EntityManager em;

	@Override
	public List<Alumno> getAlumnos() throws Exception{
		AlumnoGateway gateway = Factories.persistence.createAlumnoDao();
		return  gateway.getAlumnos(em);
	}

	@Override
	public void saveAlumno(Alumno alumno) throws Exception {
		em.persist(alumno);
	}

	@Override
	public void updateAlumno(Alumno alumno) throws Exception {
		em.merge(alumno);
	}

	@Override
	public void deleteAlumno(Long id) throws Exception {
		em.remove(this.findById(id));
	}

	@Override
	public Alumno findById(Long id) throws Exception {
		return em.find(Alumno.class, id);
	}
}
