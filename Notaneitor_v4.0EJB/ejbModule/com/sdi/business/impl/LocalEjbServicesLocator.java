package com.sdi.business.impl;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sdi.business.AlumnosService;
import com.sdi.business.ServicesFactory;

public class LocalEjbServicesLocator implements ServicesFactory {

	private static final String ALUMNOS_SERVICE_JNDI_KEY = 
			"ejb:"
			+ "Notaneitor_v4.0/"
			+ "Notaneitor_v4.0EJB/"
			+ "EjbAlumnosService!"
			+ "com.sdi.business.impl.LocalAlumnosService";

	@Override
	public AlumnosService getAlumnosService() {

		try {
			Context ctx = new InitialContext();
			return (AlumnosService) ctx.lookup(ALUMNOS_SERVICE_JNDI_KEY);

		} catch (NamingException e) {
			throw new RuntimeException("JNDI problem", e);
		}
	}

}
