package com.sdi.business.impl;

import javax.ejb.Remote;

import com.sdi.business.AlumnosService;

@Remote
public interface RemoteAlumnosService extends AlumnosService {}
